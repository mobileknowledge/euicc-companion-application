package com.nxp.euicc.companionapp.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.nxp.euicc.companionapp.R;
import com.nxp.euicc.companionapp.bluetooth.BluetoothClient;
import com.nxp.euicc.companionapp.bluetooth.BluetoothTLV;
import com.nxp.euicc.companionapp.utils.Utils;

public class QRProfileDownloadActivity extends AppCompatActivity {

    // Permission requests
    public static final int PERMISSION_REQUEST_CODE = 0x0001;

    private BluetoothClient mBluetoothClient;

    private CodeScannerView mScannerView;
    private CodeScanner mCodeScanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrreader);

        // Create the Bluetooth Client
        mBluetoothClient = BluetoothClient.getInstance(getApplicationContext());

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            initQRScan();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(mCodeScanner != null) {
            mCodeScanner.startPreview();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(mCodeScanner != null) {
            mCodeScanner.releaseResources();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                        Utils.showDialog(QRProfileDownloadActivity.this,
                                getString(R.string.app_name),
                                getString(R.string.dialog_camera_permission_error),
                                getString(R.string.dialog_accept),
                                (dialog, which) -> {
                                    // Do nothing
                                });
                    } else {
                        initQRScan();
                    }
                }
        }
    }

    private void initQRScan () {
        mScannerView = findViewById(R.id.scanner_view);
        mCodeScanner = new CodeScanner(this, mScannerView);
        mCodeScanner.setDecodeCallback(result ->
                QRProfileDownloadActivity.this.runOnUiThread(() -> {
                    Utils.showDialog(QRProfileDownloadActivity.this,
                            getString(R.string.app_name),
                            getString(R.string.dialog_profile_download),
                            getString(R.string.dialog_cancel),
                            (dialog, which) -> {
                                finish();
                            },
                            getString(R.string.dialog_accept),
                            (dialog, which) -> {
                                // Do nothing
                                downloadProfile(result.getText());
                            });

                }));

        mScannerView.setOnClickListener(view -> mCodeScanner.startPreview());
    }

    private void downloadProfile(String uri) {
        // Trigger the FW download through BLE communication
        byte[] bleData = BluetoothTLV.getTlvCommand(BluetoothTLV.PROFILE_DOWNLOAD, uri.getBytes());
        if (mBluetoothClient.isConnected()) {
            final ProgressDialog pd = Utils.showProgressDialog(QRProfileDownloadActivity.this,
                    getString(R.string.app_name),
                    getString(R.string.progress_dialog_download_profile));

            new Thread(new Runnable() {
                @Override
                public void run() {
                    byte[] response = mBluetoothClient.transmit(bleData, 90000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pd.dismiss();
                            if (response != null) {
                                int tag = response[0];
                                if (tag != BluetoothTLV.PROFILE_DOWNLOAD) {
                                    Utils.showToast(QRProfileDownloadActivity.this, "Unexpected tag value: " + tag);
                                } else {
                                    if (response[2] == '0') {
                                        Utils.showDialog(QRProfileDownloadActivity.this,
                                                getString(R.string.app_name),
                                                getString(R.string.dialog_profile_download_success),
                                                getString(R.string.dialog_accept),
                                                (dialog, which) -> {
                                                    finish();
                                                });
                                    } else {
                                        Utils.showDialog(QRProfileDownloadActivity.this,
                                                getString(R.string.app_name),
                                                getString(R.string.dialog_profile_download_fail),
                                                getString(R.string.dialog_accept),
                                                (dialog, which) -> {
                                                    finish();
                                                });
                                    }
                                }
                            } else {
                                Utils.showToast(QRProfileDownloadActivity.this, "Bluetooth device not connected");
                            }
                        }
                    });
                }
            }).start();
        } else {
            Utils.showToast(QRProfileDownloadActivity.this, "Bluetooth device not connected");
        }
    }
}
