package com.nxp.euicc.companionapp.interfaces;

import android.bluetooth.BluetoothDevice;

public interface BleInterface {

    /// Gets a value indicating the enabled or disabled status of the BLE port.
    /// Returns the Boolean value indicating enabled or disabled status of the BLE port.
    boolean isEnabled();

    /// Closes the BLE port connection, sets the isConnected property to false, and disposes of the internal Stream object.
    void close();

    /// Gets a value indicating the open or closed status of the BLE port.
    /// Returns the Boolean value indicating the open or closed status of the BLE port
    /// The isConnected property tracks whether the port is open for use by the caller, not whether the port is open by any application on the device.
    boolean isConnected();

    /**
     * Triggers the connection with the remote bluetooth device
     * Informs the user about the connection status on the listener
     *
     * @param bluetoothDevice Remote Bluetooth device
     * @param bluetoothConnectListener Listener
     */
    void connect(BluetoothDevice bluetoothDevice, BluetoothConnectListener bluetoothConnectListener);

    /// Writes a specified number of bytes to the BLE port using data from a buffer.
    /// "buffer": The byte array that contains the data to transmit.
    /// "timeout": Timeout (in msecs) before considering the operation as failed
    byte[] transmit(byte[] buffer, int timeout);

    /**
     * Starts the scanning for BLE devices
     *
     * @param BLEScanListener Listener that informs about new scanned devices
     */
    void startLeDeviceScan(BLEScanListener BLEScanListener);

    /**
     * Stops the scanning for BLE devices
     */
    void stopLeDeviceScan();

    // Interface callback for Bluetooth data read
    interface BluetoothConnectListener {
        void onConnect(boolean state);
    }

    // Interface callback for new scanned BLE devices
    interface BLEScanListener {
        void onScan(BluetoothDevice device);
    }
}
