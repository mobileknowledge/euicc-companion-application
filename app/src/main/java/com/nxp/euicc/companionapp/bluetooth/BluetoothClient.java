/*
 * Copyright (C) 2020 NXP Semiconductors
 *
 * All rights are reserved. Reproduction in whole or in part is
 * prohibited without the written consent of the copyright owner.
 *
 * NXP reserves the right to make changes without notice at any time.
 *
 * NXP makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. NXP must not be liable for any loss or damage
 * arising from its use.
 *
 */

package com.nxp.euicc.companionapp.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelUuid;
import android.util.Log;
import android.widget.Toast;

import com.nxp.euicc.companionapp.interfaces.BleInterface;
import com.nxp.euicc.companionapp.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class BluetoothClient implements BleInterface {

    protected static final String TAG = "Bluetooth LE Client";

    /// qpp start
    protected static String uuidQppService = "0000fee9-0000-1000-8000-00805f9b34fb";
    protected static String uuidQppCharWrite = "d44bc439-abfd-45a2-b575-925416129600";
    protected static String uuidQppCharRead = "d44bc439-abfd-45a2-b575-925416129601";

    // Make BluetoothClient Singleton
    private static BluetoothClient ISNTANCE = null;

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothGatt mBluetoothGatt;
    private BluetoothLeScanner mLEScanner;

    // Listeners
    private BLEScanListener mBLEScanListener;
    private BluetoothConnectListener mBluetoothConnectListener;

    private byte[] bleResponse;
    private final Object lock = new Object();

    /**
     * scan all Service ?
     */
    private boolean isConnected = false;
    private boolean qppSendDataState = false;

    private Context mContext;

    private SendThread sendDataThread = null;

    // Buffer where I get all the response script
    private byte[] mBufferDataCmd = null;

    private int mBufferOffset = 0;
    private int mExpectedSize = 0;

    public static BluetoothClient getInstance(Context ctx) {
        if (ISNTANCE == null) {
            ISNTANCE = new BluetoothClient(ctx);
        }

        return ISNTANCE;
    }

    private BluetoothClient(Context ctx) {
        this.mContext = ctx;

        this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        this.mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();

        // Set the callback to receive data
        receiveDataCallback();
    }

    public void receiveDataCallback() {
        try {
            QppApi.setCallback(new iQppCallback() {
                @Override
                public void onQppReceiveData(BluetoothGatt mBluetoothGatt, String qppUUIDForNotifyChar, byte[] qppData) {
                    readBLEData(qppData);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void readBLEData(byte[] status) {
        if (status == null) {
            Toast.makeText(mContext, "Error in the Bluetooth connection", Toast.LENGTH_LONG).show();

        } else {

            int bytesToCopy = 0;

            Log.d(TAG, "BLE Message received");

            // This is the first message that I receive for this command
            if (mBufferOffset == 0) {

                // This is the total length of the command that I will receive
                mExpectedSize = BluetoothTLV.getLength(status) + BluetoothTLV.getLengthSize(status) + 1; //1 Byte for Tag

                // I prepare the buffer size
                mBufferDataCmd = new byte[mExpectedSize];

                if (mExpectedSize > QppApi.qppServerBufferSize) {
                    bytesToCopy = QppApi.qppServerBufferSize;
                } else {
                    bytesToCopy = mExpectedSize;
                }

                // Copy the data in the first position
                System.arraycopy(status, 0, mBufferDataCmd, 0, bytesToCopy);
                //Log.d("part_data_received", arrayToHex(mBufferDataCmd));
                mBufferOffset = bytesToCopy;
            } else {
                if (mExpectedSize - mBufferOffset > QppApi.qppServerBufferSize)
                    bytesToCopy = QppApi.qppServerBufferSize;
                else
                    bytesToCopy = mExpectedSize - mBufferOffset;

                System.arraycopy(status, 0, mBufferDataCmd, mBufferOffset, bytesToCopy);
                //Log.d("part_data_received", arrayToHex(mBufferDataCmd));
                mBufferOffset = mBufferOffset + bytesToCopy;
            }

            // If we have received the whole command we can proceed
            if (mBufferOffset == mExpectedSize) {

                if (mBufferDataCmd[0] == BluetoothTLV.SEND_BLE_DISCONNECT) {
                    disconnect();
                } else {
                    // Update buffer to be transmitted to the SDK
                    synchronized (lock) {
                        bleResponse = mBufferDataCmd;
                    }
                }

                mBufferDataCmd = new byte[0];
                mBufferOffset = 0;
                mExpectedSize = 0;
            }
        }
    }

    @Override
    public byte[] transmit(byte[] data, int timeout) {
        if (isConnected) {
            clearBuffer();

            sendDataThread = new SendThread(data, true);
            sendDataThread.start();

            waitForResponse(timeout);

            if(bleResponse != null) {
                Log.d(TAG, "Ble response : " + Utils.byteArrayToHex(bleResponse));
            }

            return bleResponse;
        } else {
            return null;
        }
    }

    @Override
    public boolean isEnabled() {
        return mBluetoothAdapter.isEnabled();
    }

    @Override
    public boolean isConnected() {
        return isConnected;
    }

    @Override
    public void connect(BluetoothDevice bluetoothDevice, BluetoothConnectListener bluetoothConnectListener) {
        if (mBluetoothAdapter == null || bluetoothDevice == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return;
        }

        // Set the listener to inform about the operation result
        this.mBluetoothConnectListener = bluetoothConnectListener;

        mLEScanner.flushPendingScanResults(scanCallback);
        mLEScanner.stopScan(scanCallback);

        mBluetoothGatt = bluetoothDevice.connectGatt(mContext, false, mGattCallback);
        Log.d(TAG, "Trying to create a new connection. Gatt: " + mBluetoothGatt);
    }

    /**
     * Clears the buffer for the data to be read
     */
    private void clearBuffer() {
        synchronized (lock) {
            bleResponse = null;
        }
    }

    private class SendThread extends Thread {

        byte[] data;
        boolean sendData;

        public SendThread(byte[] data, boolean send) {
            this.data = data;
            this.sendData = send;
        }

        public void setSendData(boolean send) {
            this.sendData = send;
        }

        public void run() {
            if (data == null) {
                return;
            }

            int length = data.length;
            int count = 0;
            int offset = 0;

            Log.d(TAG, "BLE transmission starts");

            while (offset < length) {
                if (sendData) {
                    sendData = false;

                    if ((length - offset) < QppApi.qppServerBufferSize)
                        count = length - offset;
                    else
                        count = QppApi.qppServerBufferSize;

                    byte tempArray[] = new byte[count];
                    System.arraycopy(data, offset, tempArray, 0, count);
                    QppApi.qppSendData(mBluetoothGatt, tempArray);
                    offset = offset + count;
                }
            }

            Log.d(TAG, "BLE transmission finished");
        }
    }

    private void waitForResponse(int timeout) {

        long initialTimeMillis = System.currentTimeMillis();

        while (System.currentTimeMillis() - initialTimeMillis < timeout) {

            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                Log.e(TAG, "BLE Interrupted!" + e);
            }

            synchronized (lock) {
                if (bleResponse != null) break;
            }
        }
    }

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.w(TAG, "onConnectionStateChange. Status: " + status + " State: " + newState);

            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    // Look for QPP Service
                    mBluetoothGatt.discoverServices();
                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    isConnected = false;

                    if (qppSendDataState) {
                        qppSendDataState = false;
                    }

                    if (mBluetoothGatt != null) {
                        mBluetoothGatt.close();
                        mBluetoothGatt = null;
                    }

                    onConnect(false);
                }
            } else {
                if (status == 133) {
                     if (mBluetoothGatt != null) {
                        mBluetoothGatt.close();
                        mBluetoothGatt = null;
                    }

                    onConnect(false);
                }
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (QppApi.qppEnable(mBluetoothGatt, QppApi.uuidQppService, QppApi.uuidQppCharWrite)) {
                Log.d(TAG, "Succeeded to enable QPP profile");
                isConnected = true;
            } else {
                Log.d(TAG, "Failed to enable QPP profile");
                isConnected = false;
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            Log.w(TAG, "BluetoothGatt onCharacteristicChanged");
            QppApi.updateValueForNotification(gatt, characteristic);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            Log.d(TAG, "BluetoothGatt onDescriptorWrite, status: " + status);
            Log.d(TAG, "BluetoothGatt onDescriptorWrite char: " + descriptor.getCharacteristic().getUuid().toString());
            QppApi.setQppNextNotify(gatt, true);
//            if (descriptor.getCharacteristic().getUuid().toString().equals(uuidQppCharRead)) {
                onConnect(true);
//            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.w(TAG, "BluetoothGatt onCharacteristicWrite Status: " + status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                sendDataThread.setSendData(true);
            } else {
                Log.e(TAG, "Send failed!!!!");
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic, int status) {
            Log.w(TAG, "onCharacteristicRead Status: " + status);
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt,
                                     BluetoothGattDescriptor descriptor, int status) {
            Log.w(TAG, "onCharacteristicRead Status: " + status);
        }
    };

    public void disconnect() {
        if (mBluetoothAdapter == null) {
            Log.w("Qn Dbg", "BluetoothAdapter not initialized");
            return;
        }

        if (mBluetoothGatt == null) {
            Log.w("Qn Dbg", "BluetoothGatt not initialized");
            return;
        }

        Log.d(TAG, "Disconnect");
        mBluetoothGatt.disconnect();

        // We are no longer connected
        isConnected = false;
    }

    public void close() {
        Log.d(TAG, "Close Connection");

        // Disconnect and then close the connection
        disconnect();

        if (mBluetoothGatt != null) {
            mBluetoothGatt.close();
            mBluetoothGatt = null;
        }

        // We are no longer connected
        isConnected = false;
    }

    @Override
    public void startLeDeviceScan(BLEScanListener BLEScanListener) {
        this.mBLEScanListener = BLEScanListener;

        List<ScanFilter> filters = new ArrayList<>();
        ScanFilter filter = new ScanFilter.Builder().setServiceUuid(new ParcelUuid(UUID.fromString(uuidQppService))).build();
        filters.add(filter);

        ScanSettings.Builder settings = new ScanSettings.Builder();
        settings.setScanMode(ScanSettings.SCAN_MODE_BALANCED);
        settings.setReportDelay(0);

        // mLEScanner is null if application starts without BLE and Location services
        if(mLEScanner == null) {
            this.mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
        }

        mLEScanner.startScan(filters, settings.build(), scanCallback);
    }

    @Override
    public void stopLeDeviceScan() {
        mLEScanner.flushPendingScanResults(scanCallback);
        mLEScanner.stopScan(scanCallback);
    }

    // Device scan callback.
    private ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, final ScanResult result) {
            // Send callback to app on the UI Thread
            new Handler(Looper.getMainLooper()).post(() -> {
                if (mBLEScanListener != null) {
                    mBLEScanListener.onScan(result.getDevice());
                }
            });
        }
    };

    /**
     * Calls onConnect method on Listener object
     * Call is done on the Main UI Thread to get out of the Bluetooth Binder thread
     */
    private void onConnect(final boolean state) {
        // Send callback to app on the UI Thread
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                // Send callback to app on the UI Thread
                mBluetoothConnectListener.onConnect(state);
            }
        });
    }
}
