/*
 * Copyright (C) 2020 NXP Semiconductors
 *
 * All rights are reserved. Reproduction in whole or in part is
 * prohibited without the written consent of the copyright owner.
 *
 * NXP reserves the right to make changes without notice at any time.
 *
 * NXP makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. NXP must not be liable for any loss or damage
 * arising from its use.
 *
 */

package com.nxp.euicc.companionapp.bluetooth;

public class BluetoothTLV {

    public static final byte SEND_BLE_DISCONNECT = (byte) 0x01;
    public static final byte GET_EID = (byte) 0x02;
    public static final byte GET_PROFILE_LIST = (byte) 0x11;
    public static final byte PROFILE_DOWNLOAD = (byte) 0x12;
    public static final byte PROFILE_ENABLE = (byte) 0x13;
    public static final byte PROFILE_DISABLE = (byte) 0x14;
    public static final byte PROFILE_DELETE = (byte) 0x15;
    public static final byte SET_PROFILE_NICKNAME = (byte) 0x16;


    public static int getLengthSize(byte[] tlvCommand) {
        int offset = 1;
        int lenSize;

        /* Find the length in BER-TLV*/
        if (tlvCommand[offset] == (byte)0x81) {
            lenSize = 2;
        } else if (tlvCommand[offset] == (byte)0x82) {
            lenSize = 3;
        } else {
            lenSize = 1;
        }

        return lenSize;
    }

    public static int getLength(byte[] tlvCommand) {
        int length;
        int offset = 1;

        /* Find the length in BER-TLV*/
        if (tlvCommand[offset] == (byte)0x81) {
            length = tlvCommand[offset + 1];
        } else if (tlvCommand[offset] == (byte)0x82) {
            length = (tlvCommand[offset + 1]);
            length = (length << 8) & 0xFFFF;
            length = (length | (tlvCommand[offset + 2] & 0xFF));
        } else {
            length = tlvCommand[1];
        }
        return length;
    }

    public static byte[] getTlvCommand(byte type, byte[] value) {
        int lengthBytes = 0;
        int valueLength = value.length;// + 1; // one extra byte for storing subtype

        if (valueLength <= 127) {
            lengthBytes = 1;
        } else {
            if (valueLength > 255) {
                lengthBytes = 3;
            } else {
                lengthBytes = 2;
            }
        }

        byte[] dataBT = new byte[1 + lengthBytes + value.length];

        dataBT[0] = type;
        System.arraycopy(value, 0, dataBT, 1 + lengthBytes, value.length);

        if (valueLength <= 127) {
            dataBT[1] = (byte) valueLength;
        } else {
            if (valueLength > 255) {
                dataBT[1] = (byte) 0x82;
                dataBT[2] = (byte) (valueLength >> 8 & 0xFF);
                dataBT[3] = (byte) (valueLength >> 0 & 0xFF);
            } else {
                dataBT[1] = (byte) 0x81;
                dataBT[2] = (byte) valueLength;
            }
        }

        return dataBT;
    }
}
