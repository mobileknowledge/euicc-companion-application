package com.nxp.euicc.companionapp.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.nxp.euicc.companionapp.R;
import com.nxp.euicc.companionapp.utils.Utils;
import com.nxp.euicc.models.Profile;

import java.util.List;

public class ProfileAdapter extends PagerAdapter {

    private final List<Profile> profileList;
    private final Context context;
    private final ProfileItemListener profileItemListener;

    public ProfileAdapter(Context context, List<Profile> profileList, ProfileItemListener profileItemListener) {
        super();

        this.profileList = profileList;
        this.context = context;
        this.profileItemListener = profileItemListener;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return profileList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {

        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.profile_item, collection, false);

        ((TextView) layout.findViewById(R.id.profile_title)).setText(profileList.get(position).getName());
        ((TextView) layout.findViewById(R.id.profile_iccid)).setText(profileList.get(position).getIccid());
        ((TextView) layout.findViewById(R.id.profile_class)).setText(profileList.get(position).getProfileClass());
        ((TextView) layout.findViewById(R.id.profile_provider)).setText(profileList.get(position).getProviderName());
        ((TextView) layout.findViewById(R.id.profile_nickname)).setText(profileList.get(position).getProfileNickname());
        ((ImageView) layout.findViewById(R.id.profile_icon)).setImageBitmap(Utils.getProfileIcon(context, profileList.get(position)));

        ImageView deleteButton = layout.findViewById(R.id.delete_button);
        ImageView editButton = layout.findViewById(R.id.edit_button);
        Switch enableSwitch = layout.findViewById(R.id.enable_switch);

        deleteButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                Utils.showDialog(context,
                                                        context.getString(R.string.dialog_profile_delete_title),
                                                        context.getString(R.string.dialog_profile_delete_message),
                                                        context.getString(R.string.dialog_no),
                                                        new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                // Do nothing

                                                            }
                                                        },
                                                        context.getString(R.string.dialog_yes),
                                                        new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                // Proceed to delete
                                                                profileItemListener.deleteProfile(position, profileList.get(position).getIccid());
                                                            }
                                                        });
                                            }
                                        }
        );

        editButton.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {

                                              final EditText editText = new EditText(context);
                                              LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                                                      LinearLayout.LayoutParams.MATCH_PARENT,
                                                      LinearLayout.LayoutParams.MATCH_PARENT);
                                              editText.setLayoutParams(lp);

                                              Utils.showEditDialog(context,
                                                      editText,
                                                      context.getString(R.string.dialog_profile_edit_title),
                                                      context.getString(R.string.dialog_profile_edit_message),
                                                      context.getString(R.string.dialog_no),
                                                      new DialogInterface.OnClickListener() {
                                                          @Override
                                                          public void onClick(DialogInterface dialog, int which) {
                                                              // Do nothing

                                                          }
                                                      },
                                                      context.getString(R.string.dialog_yes),
                                                      new DialogInterface.OnClickListener() {
                                                          @Override
                                                          public void onClick(DialogInterface dialog, int which) {
                                                              // Proceed to edit
                                                              profileItemListener.editProfile(position, profileList.get(position).getIccid(), editText.getText().toString());
                                                          }
                                                      });
                                          }
                                      }
        );

        enableSwitch.setChecked(profileList.get(position).getState().equals("Enabled"));
        enableSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
                // Do not show the dialog if the switch value was modified programatically
                if (!buttonView.isPressed()) {
                    return;
                }

                Utils.showDialog(context,
                        isChecked ? context.getString(R.string.dialog_profile_enable_title) : context.getString(R.string.dialog_profile_disable_title),
                        isChecked ? context.getString(R.string.dialog_profile_enable_message) : context.getString(R.string.dialog_profile_disable_message),
                        context.getString(R.string.dialog_no),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                enableSwitch.setChecked(!enableSwitch.isChecked());
                            }
                        },
                        context.getString(R.string.dialog_yes),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Proceed to enable or disable
                                if (isChecked) {
                                    profileItemListener.enableProfile(position, profileList.get(position).getIccid());
                                } else {
                                    profileItemListener.disableProfile(position, profileList.get(position).getIccid());
                                }
                            }
                        });

            }
        });

        collection.addView(layout);
        return layout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public void restoreState(Parcelable arg0, ClassLoader arg1) {
        // TODO Auto-generated method stub

    }

    @Override
    public Parcelable saveState() {
        // TODO Auto-generated method stub
        return null;
    }

    public interface ProfileItemListener {
        void deleteProfile(int position, String iccid);
        void enableProfile(int position, String iccid);
        void disableProfile(int position, String iccid);
        void editProfile(int position, String iccid, String nickname);
    }
}
