package com.nxp.euicc.companionapp.activities;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.nxp.euicc.companionapp.R;
import com.nxp.euicc.companionapp.adapter.BLEItemAdapter;
import com.nxp.euicc.companionapp.bluetooth.BluetoothClient;
import com.nxp.euicc.companionapp.interfaces.BleInterface;
import com.nxp.euicc.companionapp.utils.Utils;

import java.util.ArrayList;

public class ScanViewActivity extends AppCompatActivity implements BLEItemAdapter.BleItemSelectListener {

    private static String TAG = ScanViewActivity.class.getName();

    // Permission requests
    public static final int PERMISSION_REQUEST_CODE = 0x0001;
    public static final int ENABLE_BT_REQUEST_CODE = 0x0002;
    public static final int ENABLE_LOCATION_REQUEST_CODE = 0x0003;

    // Stops scanning after 20 seconds.
    private static final long SCAN_PERIOD = 20000;

    private boolean mScanning;
    private Handler mHandler;

    // Member fields
    private BLEItemAdapter mNewDevicesNameArrayAdapter;
    private ArrayList<BluetoothDevice> BluetoothDeviceList;

    // UI elements
    private ListView newDevicesListView;
    private Button scanButton;
    private TextView txtScan;

    // Make the Bluetooth Client static so that all activities can call it
    public BluetoothClient mBluetoothClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanview);

        initViews();
        initBle();
        initLocation();

        mHandler = new Handler();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Stop scanning when the app is closed
        if (mBluetoothClient != null) {
            mHandler.removeCallbacksAndMessages(null);

            mBluetoothClient.stopLeDeviceScan();
            mBluetoothClient.close();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case ENABLE_BT_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    // Do nothing, Scan is triggered by the user

                } else {
                    Utils.showDialog(ScanViewActivity.this,
                            getString(R.string.app_name),
                            getString(R.string.dialog_bluetooth_disabled_error),
                            getString(R.string.dialog_accept),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // Do nothing
                                }
                            });
                }

                break;

            case ENABLE_LOCATION_REQUEST_CODE:
                if (!checkLocation()) {
                    Utils.showDialog(ScanViewActivity.this,
                            getString(R.string.app_name),
                            getString(R.string.dialog_location_disabled_error),
                            getString(R.string.dialog_accept),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // Do nothing
                                }
                            });
                }

                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                        Utils.showDialog(ScanViewActivity.this,
                                getString(R.string.app_name),
                                getString(R.string.dialog_ble_scan_permission_error),
                                getString(R.string.dialog_accept),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Do nothing
                                    }
                                });
                    } else {
                        // Stops scanning after a pre-defined scan period.
                        mHandler.postDelayed(this::stopDeviceDiscoverBluetooth, SCAN_PERIOD);
                        startDeviceDiscoverBluetooth();
                    }
                }
        }
    }

    private void initBle() {
        mBluetoothClient = BluetoothClient.getInstance(getApplicationContext());
        if (!mBluetoothClient.isEnabled()) {
            Log.d(TAG, "Bluetooth is currently disabled... enabling");
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, ENABLE_BT_REQUEST_CODE);
        }
    }

    private void initLocation() {
        if (!checkLocation()) {
            Utils.showDialog(ScanViewActivity.this,
                    getString(R.string.app_name),
                    getString(R.string.dialog_location_enable_request),
                    getString(R.string.dialog_cancel),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Utils.showDialog(ScanViewActivity.this,
                                    getString(R.string.app_name),
                                    getString(R.string.dialog_location_disabled_error),
                                    getString(R.string.dialog_accept),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // Do nothing
                                        }
                                    });
                        }
                    },
                    getString(R.string.dialog_accept),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // if this button is clicked, go to Settings
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), ENABLE_LOCATION_REQUEST_CODE);
                        }
                    });
        }
    }

    private boolean checkLocation() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void initViews() {
        newDevicesListView = findViewById(R.id.new_devices);
        scanButton = findViewById(R.id.server_scan_button);
        txtScan = findViewById(R.id.text_scan_devices);

        // Initialize array adapters.
        BluetoothDeviceList = new ArrayList<>();
        mNewDevicesNameArrayAdapter = new BLEItemAdapter(this, BluetoothDeviceList, this);
        newDevicesListView.setAdapter(mNewDevicesNameArrayAdapter);

        // Initialize the button to perform device discovery
        scanButton.setOnClickListener(v -> {

            if (mScanning) {
                mHandler.removeCallbacksAndMessages(null);
                stopDeviceDiscoverBluetooth();
            } else {
                if (mBluetoothClient.isEnabled()) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        if (checkLocation()) {
                            mHandler.postDelayed(this::stopDeviceDiscoverBluetooth, SCAN_PERIOD);
                            startDeviceDiscoverBluetooth();
                        } else {
                            Utils.showDialog(ScanViewActivity.this,
                                    getString(R.string.app_name),
                                    getString(R.string.dialog_location_enable_request),
                                    getString(R.string.dialog_cancel),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Utils.showDialog(ScanViewActivity.this,
                                                    getString(R.string.app_name),
                                                    getString(R.string.dialog_location_disabled_error),
                                                    getString(R.string.dialog_accept),
                                                    new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            // Do nothing
                                                        }
                                                    });
                                        }
                                    },
                                    getString(R.string.dialog_accept),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // if this button is clicked, go to Settings
                                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), ENABLE_LOCATION_REQUEST_CODE);
                                        }
                                    });
                        }
                    } else {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
                    }
                } else {
                    Log.d(TAG, "Bluetooth is currently disabled... enabling");
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, ENABLE_BT_REQUEST_CODE);
                }
            }
        });

        // App version dialog
        findViewById(R.id.img_info).setOnClickListener(view -> {
            Utils.showDialog(ScanViewActivity.this,
                    getString(R.string.app_name),
                    getString(R.string.dialog_app_version),
                    getString(R.string.dialog_accept),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing
                        }
                    });
        });
    }

    private void startDeviceDiscoverBluetooth() {
        // Indicate scanning in the title
        txtScan.setText(R.string.txt_scanning);
        scanButton.setText(R.string.btn_stop_scan);
        mScanning = true;

        // Clear all the discovered devices
        BluetoothDeviceList.clear();
        mNewDevicesNameArrayAdapter.clear();

        // Initializes list view adapter.
        mBluetoothClient.startLeDeviceScan(device -> {

            // Ignore devices that do not define name or address
            if ((device.getName() == null) || (device.getAddress() == null)) {
                return;
            }

            Log.d(TAG, "New scanned device" + device.getName());
            if (!BluetoothDeviceList.contains(device)) {
                BluetoothDeviceList.add(device);
                mNewDevicesNameArrayAdapter.notifyDataSetChanged();
            }
        });
    }

    private void stopDeviceDiscoverBluetooth() {
        mScanning = false;
        mBluetoothClient.stopLeDeviceScan();

        txtScan.setText(R.string.txt_start_scan);
        scanButton.setText(R.string.btn_start_scan);
    }

    private void connectToDevice(BluetoothDevice bluetoothDevice) {
        Log.d(TAG, "Connect to BLE Device name:" + bluetoothDevice.getName());
        Log.d(TAG, "Connect to BLE Device address:" + bluetoothDevice.getAddress());

        findViewById(R.id.progress_bar_scanview).setVisibility(View.VISIBLE);
        mBluetoothClient.connect(bluetoothDevice, new BleInterface.BluetoothConnectListener() {
            @Override
            public void onConnect(boolean state) {
                findViewById(R.id.progress_bar_scanview).setVisibility(View.GONE);

                if (state) {
                    Log.d(TAG, "BLE connection process completed");
                    // Toast.makeText(ScanViewActivity.this, "BLE connection established", Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(ScanViewActivity.this, AvailableOptionsActivity.class);
                    startActivity(intent);
                } else {
                    Log.d(TAG, "BLE connection process failed");
                    Toast.makeText(ScanViewActivity.this, "BLE connection failed", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void BleItemSelected(BluetoothDevice bluetoothDevice) {
        stopDeviceDiscoverBluetooth();
        connectToDevice(bluetoothDevice);
    }
}
