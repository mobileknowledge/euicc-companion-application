package com.nxp.euicc.companionapp.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.nxp.euicc.companionapp.R;
import com.nxp.euicc.companionapp.bluetooth.BluetoothClient;
import com.nxp.euicc.companionapp.bluetooth.BluetoothTLV;
import com.nxp.euicc.companionapp.utils.Utils;
import com.nxp.euicc.models.Profile;

import org.apache.commons.lang3.SerializationUtils;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

public class AvailableOptionsActivity extends AppCompatActivity implements View.OnClickListener {

    private static String TAG = AvailableOptionsActivity.class.getName();

    private BluetoothClient mBluetoothClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_available_options);

        findViewById(R.id.get_id_layout).setOnClickListener(this);
        findViewById(R.id.list_profiles_layout).setOnClickListener(this);
        findViewById(R.id.qr_scan_layout).setOnClickListener(this);
        findViewById(R.id.disconnect_button).setOnClickListener(this);

        // Create the Bluetooth Client
        mBluetoothClient = BluetoothClient.getInstance(getApplicationContext());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.get_id_layout) {
            getEID();
        }

        if (view.getId() == R.id.list_profiles_layout) {
            getProfileList();
        }

        if (view.getId() == R.id.qr_scan_layout) {
            Intent intent = new Intent(AvailableOptionsActivity.this, QRProfileDownloadActivity.class);
            startActivity(intent);
        }

        if (view.getId() == R.id.disconnect_button) {
            mBluetoothClient.close();
            finish();
        }
    }

    private void getEID() {
        byte[] buffer = {0x00};
        byte[] bleData = BluetoothTLV.getTlvCommand(BluetoothTLV.GET_EID, buffer);
        Log.d(TAG, "Ble Data with TLV: " + Utils.byteArrayToHex(bleData));

        if (mBluetoothClient.isConnected()) {
            final ProgressDialog pd = Utils.showProgressDialog(AvailableOptionsActivity.this,
                    getString(R.string.app_name),
                    getString(R.string.progress_dialog_retrieve_eid));

            new Thread(new Runnable() {
                @Override
                public void run() {
                    byte[] response = mBluetoothClient.transmit(bleData, 10000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pd.dismiss();
                            if (response != null) {
                                int tag = response[0];
                                if (tag != BluetoothTLV.GET_EID) {
                                    Utils.showToast(AvailableOptionsActivity.this, "Unexpected tag value: " + tag);
                                } else {
                                    if (response.length > 3) {
                                        //int length = result[1];
                                        byte[] value = new byte[BluetoothTLV.getLength(response)];
                                        int lenSize = BluetoothTLV.getLengthSize(response);
                                        System.arraycopy(response, 1 + lenSize, value, 0, BluetoothTLV.getLength(response));
                                        final String eid = new String(value, StandardCharsets.UTF_8);

                                        Utils.showDialog(AvailableOptionsActivity.this,
                                                getString(R.string.app_name),
                                                "EID: " + eid,
                                                getString(R.string.dialog_accept),
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        // Do nothing
                                                    }
                                                });
                                    } else {
                                        Utils.showToast(AvailableOptionsActivity.this, "Failed, Please try again!");
                                    }
                                }
                            } else {
                                Utils.showToast(AvailableOptionsActivity.this, "Bluetooth device not connected");
                            }
                        }
                    });
                }
            }).start();
        } else {
            Utils.showToast(AvailableOptionsActivity.this, "Bluetooth device not connected");
        }
    }

    private void getProfileList() {
        byte[] buffer = {0x00};
        byte[] bleData = BluetoothTLV.getTlvCommand(BluetoothTLV.GET_PROFILE_LIST, buffer);
        if (mBluetoothClient.isConnected()) {
            final ProgressDialog pd = Utils.showProgressDialog(AvailableOptionsActivity.this,
                    getString(R.string.app_name),
                    getString(R.string.progress_dialog_retrieve_profiles_list));

            new Thread(new Runnable() {
                @Override
                public void run() {
                    byte[] response = mBluetoothClient.transmit(bleData, 30000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pd.dismiss();
                            if (response != null) {
                                int tag = response[0];
                                if (tag != BluetoothTLV.GET_PROFILE_LIST) {
                                    Utils.showToast(AvailableOptionsActivity.this, "Unexpected tag value: " + tag);
                                } else {
                                    byte[] value = new byte[BluetoothTLV.getLength(response)];
                                    int lenSize = BluetoothTLV.getLengthSize(response);
                                    System.arraycopy(response, 1 + lenSize, value, 0, BluetoothTLV.getLength(response));
                                    Log.d(TAG, "Value: " + Utils.byteArrayToHex(value));

                                    try {
                                        if (value.length > (byte) 0x01) {
                                            Object[] deserialList = SerializationUtils.deserialize(value);
                                            List<Profile> profileArrayList = (List<Profile>) (Object) Arrays.asList(deserialList);
                                            Log.d(TAG, "Number of profiles received: " + profileArrayList.size());

                                            Intent intent = new Intent(AvailableOptionsActivity.this, ProfileManagerActivity.class);
                                            intent.putExtra(ProfileManagerActivity.EXTRA_PROFILE, profileArrayList.toArray(new Profile[0]));
                                            startActivity(intent);
                                        } else {
                                            if (value[0] == (byte) 0x00) {
                                                Utils.showDialog(AvailableOptionsActivity.this,
                                                        getString(R.string.app_name),
                                                        getString(R.string.dialog_profile_empty_list),
                                                        getString(R.string.dialog_accept),
                                                        new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                // Do nothing
                                                            }
                                                        });
                                            } else {
                                                Utils.showToast(AvailableOptionsActivity.this, "Failed, Please try again!");
                                            }
                                        }
                                    } catch (Exception e) {
                                        Log.v(TAG, " List Profile Failed, Exception ::" + e.getMessage());
                                    }
                                }
                            } else {
                                Utils.showToast(AvailableOptionsActivity.this, "Bluetooth device not connected");
                            }
                        }
                    });
                }
            }).start();
        } else {
            Utils.showToast(AvailableOptionsActivity.this, "Bluetooth device not connected");
        }
    }
}
