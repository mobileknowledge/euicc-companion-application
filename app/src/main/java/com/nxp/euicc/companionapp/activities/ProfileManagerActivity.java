package com.nxp.euicc.companionapp.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.nxp.euicc.companionapp.R;
import com.nxp.euicc.companionapp.adapter.ProfileAdapter;
import com.nxp.euicc.companionapp.bluetooth.BluetoothClient;
import com.nxp.euicc.companionapp.bluetooth.BluetoothTLV;
import com.nxp.euicc.companionapp.utils.Utils;
import com.nxp.euicc.companionapp.utils.WrapContentHeightViewPager;
import com.nxp.euicc.models.Profile;
import com.viewpagerindicator.LinePageIndicator;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ProfileManagerActivity extends AppCompatActivity implements ProfileAdapter.ProfileItemListener {

    private static String TAG = ProfileManagerActivity.class.getName();

    public static final String EXTRA_PROFILE = "com.nxp.euicc.companionapp.PROFILE";

    private BluetoothClient mBluetoothClient;
    private List<Profile> mProfileList;

    private ProfileAdapter profileAdapter;
    private LinePageIndicator linePageIndicator;
    private WrapContentHeightViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        // Create the Bluetooth Client
        mBluetoothClient = BluetoothClient.getInstance(getApplicationContext());

        // Get the profile details
        Profile[] profileArr = (Profile[]) getIntent().getSerializableExtra(EXTRA_PROFILE);
        if (profileArr != null) {
            mProfileList = new LinkedList<>(Arrays.asList(profileArr));
            Log.d(TAG, "Number of profiles received: " + mProfileList.size());
        }

        initViews();
    }

    private void initViews() {
        viewPager = findViewById(R.id.viewpager);
        linePageIndicator = findViewById(R.id.viewpagerindicator);
        linePageIndicator.setLineWidth(60.0f);
        linePageIndicator.setStrokeWidth(20.0f);
        linePageIndicator.setSelectedColor(Color.BLUE);

        profileAdapter = new ProfileAdapter(ProfileManagerActivity.this, mProfileList, this);
        viewPager.setAdapter(profileAdapter);
        linePageIndicator.setViewPager(viewPager, 0);
    }

    private void notifyDataSetChanged() {
        profileAdapter = new ProfileAdapter(ProfileManagerActivity.this, mProfileList, this);
        viewPager.setAdapter(profileAdapter);
        linePageIndicator.setViewPager(viewPager, 0);
        linePageIndicator.notifyDataSetChanged();
   }

    @Override
    public void deleteProfile(int position, String iccid) {
        byte[] bleData = BluetoothTLV.getTlvCommand(BluetoothTLV.PROFILE_DELETE, iccid.getBytes());
        Log.d(TAG, "Ble Data with TLV: " + Utils.byteArrayToHex(bleData));

        if (mBluetoothClient.isConnected()) {
            final ProgressDialog pd = Utils.showProgressDialog(ProfileManagerActivity.this,
                    getString(R.string.app_name),
                    getString(R.string.progress_dialog_delete_profile));

            new Thread(new Runnable() {
                @Override
                public void run() {
                    byte[] response = mBluetoothClient.transmit(bleData, 60000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pd.dismiss();
                            if (response != null) {
                                int tag = response[0];
                                if (tag != BluetoothTLV.PROFILE_DELETE) {
                                    Utils.showToast(ProfileManagerActivity.this, "Unexpected tag value: " + tag);
                                } else {
                                    if (response[2] == '0') {
                                        Log.v(TAG, "Profile Deleted Successfully");
                                        mProfileList.remove(position);

                                        Utils.showDialog(ProfileManagerActivity.this,
                                                getString(R.string.app_name),
                                                getString(R.string.dialog_profile_deleted),
                                                getString(R.string.dialog_accept),
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        notifyDataSetChanged();

                                                        // Finish the activity if there are no more profiles
                                                        if(mProfileList.size() == 0) {
                                                            finish();
                                                        }
                                                    }
                                                });
                                    } else {
                                        Log.v(TAG, "Profile Deletion Failed");
                                        Utils.showToast(ProfileManagerActivity.this, "Failed to delete profile!");
                                        notifyDataSetChanged();
                                    }
                                }
                            } else {
                                Utils.showToast(ProfileManagerActivity.this, "Bluetooth device not connected");
                                notifyDataSetChanged();
                            }
                        }
                    });
                }
            }).start();
        } else {
            Utils.showToast(ProfileManagerActivity.this, "Bluetooth device not connected");
            notifyDataSetChanged();
        }
    }

    @Override
    public void enableProfile(int position, String iccid) {
        // byte[] buffer = Utils.hexStringToByteArray(toHex(iccid, iccid.length()));
        byte[] bleData = BluetoothTLV.getTlvCommand(BluetoothTLV.PROFILE_ENABLE, iccid.getBytes());
        Log.d(TAG, "Ble Data with TLV: " + Utils.byteArrayToHex(bleData));

        if (mBluetoothClient.isConnected()) {
            final ProgressDialog pd = Utils.showProgressDialog(ProfileManagerActivity.this,
                    getString(R.string.app_name),
                    getString(R.string.progress_dialog_enable_profile));

            new Thread(new Runnable() {
                @Override
                public void run() {
                    byte[] response = mBluetoothClient.transmit(bleData, 30000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pd.dismiss();
                            if (response != null) {
                                int tag = response[0];
                                if (tag != BluetoothTLV.PROFILE_ENABLE && tag != BluetoothTLV.PROFILE_DISABLE) {
                                    Utils.showToast(ProfileManagerActivity.this, "Unexpected tag value: " + tag);
                                } else {
                                    if (response[2] == '0') {
                                        Log.v(TAG, "Profile enabled Successfully");
                                        mProfileList.get(position).setState("Enabled");

                                        Utils.showDialog(ProfileManagerActivity.this,
                                                getString(R.string.app_name),
                                                getString(R.string.dialog_profile_enabled),
                                                getString(R.string.dialog_accept),
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        notifyDataSetChanged();
                                                    }
                                                });
                                    } else {
                                        Log.v(TAG, "Profile enable Failed");
                                        Utils.showToast(ProfileManagerActivity.this, "Failed to enable profile!");
                                        notifyDataSetChanged();
                                    }
                                }
                            } else {
                                Utils.showToast(ProfileManagerActivity.this, "Bluetooth device not connected");
                                notifyDataSetChanged();
                            }
                        }
                    });
                }
            }).start();
        } else {
            Utils.showToast(ProfileManagerActivity.this, "Bluetooth device not connected");
            notifyDataSetChanged();
        }
    }

    @Override
    public void disableProfile(int position, String iccid) {
        // byte[] buffer = Utils.hexStringToByteArray(toHex(iccid, iccid.length()));
        byte[] bleData = BluetoothTLV.getTlvCommand(BluetoothTLV.PROFILE_DISABLE, iccid.getBytes());
        Log.d(TAG, "Ble Data with TLV: " + Utils.byteArrayToHex(bleData));

        if (mBluetoothClient.isConnected()) {
            final ProgressDialog pd = Utils.showProgressDialog(ProfileManagerActivity.this,
                    getString(R.string.app_name),
                    getString(R.string.progress_dialog_disable_profile));

            new Thread(new Runnable() {
                @Override
                public void run() {
                    byte[] response = mBluetoothClient.transmit(bleData, 30000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pd.dismiss();
                            if (response != null) {
                                int tag = response[0];
                                if (tag != BluetoothTLV.PROFILE_ENABLE && tag != BluetoothTLV.PROFILE_DISABLE) {
                                    Utils.showToast(ProfileManagerActivity.this, "Unexpected tag value: " + tag);
                                } else {
                                    if (response[2] == '0') {
                                        Log.v(TAG, "Profile enabled Successfully");
                                        mProfileList.get(position).setState("Disabled");

                                        Utils.showDialog(ProfileManagerActivity.this,
                                                getString(R.string.app_name),
                                                getString(R.string.dialog_profile_disabled),
                                                getString(R.string.dialog_accept),
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        notifyDataSetChanged();
                                                    }
                                                });
                                    } else {
                                        Log.v(TAG, "Profile enable Failed");
                                        Utils.showToast(ProfileManagerActivity.this, "Failed to disable profile!");
                                        notifyDataSetChanged();
                                    }
                                }
                            } else {
                                Utils.showToast(ProfileManagerActivity.this, "Bluetooth device not connected");
                                notifyDataSetChanged();
                            }
                        }
                    });
                }
            }).start();
        } else {
            Utils.showToast(ProfileManagerActivity.this, "Bluetooth device not connected");
            notifyDataSetChanged();
        }
    }

    @Override
    public void editProfile(int position, String iccid, String nickname) {
        // Added logic to send ICCID which was not there in reference LPA Proxy App
        byte[] iccidBytes = BluetoothTLV.getTlvCommand((byte) 0x01, iccid.getBytes());
        byte[] nicknameBytes = BluetoothTLV.getTlvCommand((byte) 0x02, nickname.getBytes());

        byte[] bleData = BluetoothTLV.getTlvCommand(BluetoothTLV.SET_PROFILE_NICKNAME, Utils.concat(iccidBytes, nicknameBytes));
        Log.d(TAG, "Ble Data with TLV: " + Utils.byteArrayToHex(bleData));

        if (mBluetoothClient.isConnected()) {
            final ProgressDialog pd = Utils.showProgressDialog(ProfileManagerActivity.this,
                    getString(R.string.app_name),
                    getString(R.string.progress_dialog_edit_profile));

            new Thread(new Runnable() {
                @Override
                public void run() {
                    byte[] response = mBluetoothClient.transmit(bleData, 10000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pd.dismiss();
                            if (response != null) {
                                int tag = response[0];
                                if (tag != BluetoothTLV.SET_PROFILE_NICKNAME) {
                                    Utils.showToast(ProfileManagerActivity.this, "Unexpected tag value: " + tag);
                                } else {
                                    byte[] value = new byte[BluetoothTLV.getLength(response)];
                                    int lenSize = BluetoothTLV.getLengthSize(response);
                                    System.arraycopy(response, 1 + lenSize, value, 0, BluetoothTLV.getLength(response));

                                    if (value.length > (byte) 0x00) {
                                        if (value[0] == '0') {
                                            Log.v(TAG, "Profile edited Successfully");
                                            mProfileList.get(position).setProfileNickname(nickname);

                                            Utils.showDialog(ProfileManagerActivity.this,
                                                    getString(R.string.app_name),
                                                    getString(R.string.dialog_profile_edited),
                                                    getString(R.string.dialog_accept),
                                                    new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            notifyDataSetChanged();
                                                        }
                                                    });
                                        } else {
                                            Log.v(TAG, "Profile edit Failed!");
                                            Utils.showToast(ProfileManagerActivity.this, "Edit profile failed");
                                        }
                                    } else {
                                        Utils.showToast(ProfileManagerActivity.this, "Edit profile failed");
                                    }
                                }
                            } else {
                                Utils.showToast(ProfileManagerActivity.this, "Bluetooth device not connected");
                            }
                        }
                    });
                }
            }).start();
        } else {
            Utils.showToast(ProfileManagerActivity.this, "Bluetooth device not connected");
        }
    }
}
