package com.nxp.euicc.companionapp.adapter;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nxp.euicc.companionapp.R;

import java.util.ArrayList;

public class BLEItemAdapter extends BaseAdapter {

    private static final String TAG = "BLEItemAdapter";

    static class ViewHolder {
        TextView device_name;
        TextView device_mac_address;
        LinearLayout adapter_layout;
    }

    // Listeners
    BleItemSelectListener BleItemSelectListener;

    private static int convertViewCounter = 0;
    private ArrayList<BluetoothDevice> bluetoothDeviceArrayList;
    private LayoutInflater inflater;

    public BLEItemAdapter(Context c, ArrayList<BluetoothDevice> bluetoothDeviceArrayList, BleItemSelectListener BleItemSelectListener) {
        this.inflater = LayoutInflater.from(c);
        this.bluetoothDeviceArrayList = bluetoothDeviceArrayList;
        this.BleItemSelectListener = BleItemSelectListener;
    }

    public void clear() {
        bluetoothDeviceArrayList.clear();
    }

    @Override
    public int getCount() {
        return bluetoothDeviceArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return bluetoothDeviceArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        Log.v(TAG, "in getView for position " + position + ", convertView is "
                + ((convertView == null) ? "null" : "being recycled"));

        if (convertView == null) {
            Log.v(TAG, convertViewCounter + " convertViews have been created");

            convertView = inflater.inflate(R.layout.ble_item, parent, false);
            convertViewCounter++;

            holder = new ViewHolder();
            holder.device_name = convertView.findViewById(R.id.device_name);
            holder.device_mac_address = convertView.findViewById(R.id.device_mac_address);
            holder.adapter_layout = convertView.findViewById(R.id.list_bluetooth_item);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final BluetoothDevice device = bluetoothDeviceArrayList.get(position);

        // Setting all values in listview
        holder.device_name.setText(device.getName());
        holder.device_mac_address.setText(device.getAddress());
        holder.adapter_layout.setTag(holder);
        convertView.setTag(holder);

        holder.adapter_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BleItemSelectListener.BleItemSelected(device);
            }
        });

        return convertView;
    }

    public interface BleItemSelectListener {
        public void BleItemSelected(BluetoothDevice bluetoothDevice);
    }
}