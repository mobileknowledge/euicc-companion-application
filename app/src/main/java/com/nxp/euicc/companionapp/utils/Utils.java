package com.nxp.euicc.companionapp.utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.EditText;
import android.widget.Toast;

import com.nxp.euicc.companionapp.R;
import com.nxp.euicc.models.Profile;

public class Utils {

    /**
     * Shows toast
     *
     * @param context             Context
     * @param message             Message
     *
     */
    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    /**
     * Shows dialog with only positive button
     *
     * @param context             Context
     * @param title               Dialog Title
     * @param message             Dialog Message
     * @param rightButtonTxt      Positive button text
     * @param rightButtonListener Positive button listener
     */
    public static void showDialog(Context context, String title, String message, String rightButtonTxt,
                           final DialogInterface.OnClickListener rightButtonListener) {
        showDialog(context, title, message, null, null, rightButtonTxt, rightButtonListener);
    }

    /**
     * Shows dialog with both positive and negative buttons
     *
     * @param context             Context
     * @param title               Dialog Title
     * @param message             Dialog Message
     * @param leftButtonTxt       Negative button listener
     * @param leftButtonListener  Negative button listener
     * @param rightButtonTxt      Positive button text
     * @param rightButtonListener Positive button listener
     */
    public static void showDialog(Context context, String title, String message, final String leftButtonTxt, final DialogInterface.OnClickListener leftButtonListener,
                           final String rightButtonTxt, final DialogInterface.OnClickListener rightButtonListener) {

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        if (leftButtonTxt != null && !leftButtonTxt.isEmpty()) {
            builder.setNegativeButton(leftButtonTxt, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();

                    if (leftButtonListener != null) {
                        leftButtonListener.onClick(dialog, id);
                    }
                }
            });
        }

        if (rightButtonTxt != null && !rightButtonTxt.isEmpty()) {
            builder.setPositiveButton(rightButtonTxt, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();

                    if (rightButtonListener != null) {
                        rightButtonListener.onClick(dialog, id);
                    }
                }
            });
        }

        // Create the AlertDialog object and return it
        builder.create();
        builder.show();
    }

    /**
     * Shows dialog with both positive and negative buttons
     *
     * @param context             Context
     * @param editText            Input EditText
     * @param title               Dialog Title
     * @param message             Dialog Message
     * @param leftButtonTxt       Negative button listener
     * @param leftButtonListener  Negative button listener
     * @param rightButtonTxt      Positive button text
     * @param rightButtonListener Positive button listener
     */
    public static void showEditDialog(Context context, EditText editText, String title, String message, final String leftButtonTxt, final DialogInterface.OnClickListener leftButtonListener,
                                 final String rightButtonTxt, final DialogInterface.OnClickListener rightButtonListener) {

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setView(editText);

        if (leftButtonTxt != null && !leftButtonTxt.isEmpty()) {
            builder.setNegativeButton(leftButtonTxt, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();

                    if (leftButtonListener != null) {
                        leftButtonListener.onClick(dialog, id);
                    }
                }
            });
        }

        if (rightButtonTxt != null && !rightButtonTxt.isEmpty()) {
            builder.setPositiveButton(rightButtonTxt, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();

                    if (rightButtonListener != null) {
                        rightButtonListener.onClick(dialog, id);
                    }
                }
            });
        }

        // Create the AlertDialog object and return it
        builder.create();
        builder.show();
    }

    /**
     * Shows progress dialog
     *
     * @param context             Context
     * @param title               Dialog Title
     * @param message             Dialog Message
     *
     * @return ProgresDialog
     */
    public static ProgressDialog showProgressDialog(Context context, String title, String message) {

        // Use the Builder class for convenient dialog construction
        return ProgressDialog.show(context, title, message);
    }

    /**
     * Concatenates the two given arrays
     *
     * @param b1
     *            the first byte array
     * @param b2
     *            the second byte array
     * @return the resulting byte array
     */
    public static byte[] concat(byte[] b1, byte[] b2) {
        if (b1 == null) {
            return b2;
        } else if (b2 == null) {
            return b1;
        } else {
            byte[] result = new byte[b1.length + b2.length];
            System.arraycopy(b1, 0, result, 0, b1.length);
            System.arraycopy(b2, 0, result, b1.length, b2.length);
            return result;
        }
    }

    public static String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder();
        for(byte b: a)
            sb.append(String.format("%02X", b&0xff));
        return sb.toString();
    }

    public static byte[] hexStringToByteArray(String s) {
        byte[] b = new byte[s.length() / 2];
        for (int i = 0; i < b.length; i++) {
            int index = i * 2;
            int v = Integer.parseInt(s.substring(index, index + 2), 16);
            b[i] = (byte) v;
        }
        return b;
    }

    public static Bitmap getProfileIcon(Context context, Profile profile) {
        Bitmap profileIconBitmap;
        if(profile.getName().equalsIgnoreCase("nxp")) {
            profileIconBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.nxp_profile_icon);
        } else {
            if (profile.getProfileIcon() != null && !profile.getProfileIcon().isEmpty()) {
                byte[] byteArray = Utils.hexStringToByteArray(profile.getProfileIcon());
                profileIconBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            } else {
                profileIconBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.mno_generic_profile_icon);
            }
        }

        return profileIconBitmap;
    }
}
